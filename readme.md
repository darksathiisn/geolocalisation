# <img src="images/gps.png" width="100"> **La géolocalisation**

### ![gpslogo](images/gpslogo.png) [Un trésor à Valenciennes](tresor.md)

### ![gpslogo](images/gpslogo.png) [Le GPS comment ça marche](trameNMEA.md)

### ![gpslogo](images/gpslogo.png) [OpenStreetMap et Python](openStreetMap_Python.md)