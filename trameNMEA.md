# ![gpsTitre](images/gpstitre.png) **Le GPS, comment ça marche ?**

![gpsLogo](images/gpslogo.png) GPS Test: Je vois les satellites depuis mon smartphone.

<img src="images/gpsTestApplication.png" width="200px">

<img src="images/gps1.jpg" width="300px">
<img src="images/gps2.jpg" width="300px">
<img src="images/gps3.jpg" width="300px">


![gpsLogo](images/gpslogo.png) **PROJET 1** : Visionne la vidéo ci-dessous sur le fonctionnement du GPS.

[![image](images/GPSvideo.png)](https://youtu.be/K4BFxwCCx_A)

- Pour faire simple, le GPS est un système qui a trois parties basiques : les satellites, les stations au sol et les récepteurs.

- Les stations au sol utilisent des radars pour vérifier si les satellites sont vraiment là où ils sont supposés être.

- Le système GPS a 32 satellites actifs en orbite autour de la Terre. 24 d’entre eux sont des satellites-base, et le reste sert pour les remplacement d’urgence quand il arrive quelque chose aux autres.

- Un récepteur sur Terre doit voir au moins 4 satellites pour calculer une position exacte car le GPS utilise le mécanisme de trilatération.

- La trilatération 2-D calcule la latitude et la longitude sur une carte.

- Quand il s’agit de trilatération 3-D, c’est à peu près la même chose, mais il y aura des sphères au lieu de cercles. La position 3-D inclut ta latitude, ta longitude et ton altitude.

- Les satellites GPS envoient des informations à propos de leur position et l’heure exacte à un récepteur GPS à certains intervalles. Le récepteur obtient l’information sous la forme d’un signal.

- Les satellites GPS ont des horloges atomiques qui montrent l’heure la plus précise, mais il serait impossible d’installer ces horloges dans chaque récepteur.

- Les horloges atomiques des satellites avancent de 38 microsecondes par rapport aux horloges classiques chaque jour. Si les scientifiques ne faisaient rien à ce sujet, les localisations GPS s’éteindraient de près de 10 km par jour.

- Il existe aussi un calendrier GPS dans le récepteur qui garde la trace de tous les satellites.

- Le GPS détermine non seulement la position exacte des gens et des objets, mais il envoie aussi des signaux temporels qui sont exacts à 10 milliardièmes de secondes.

- Même s’ils sont incroyablement précis et utiles, parfois, le GPS amène les gens dans des endroits inattendus, surtout dans les zones rurales.


#  ![gpsTitre](images/gpstitre.png) Géolocalisation : trame NMEA

Les différents composants dʼun appareil électronique (ex : un
téléphone mobile) communiquent par des **protocoles normalisés**. Ainsi, les puces GPS qui effectuent les calculs de positionnement envoient leurs résultats présentés suivant une trame
normalisée : **la trame NMEA 0183**. Le développeur dʼun dispositif souhaitant interroger un GPS sait quʼil pourra exploiter cette
trame pour obtenir des informations de date et de position.


![gpsLogo](images/gpslogo.png) **QUESTION 1** : Que désigne chacun des noms suivants : `Beidou - Galileo - GPS - GLONASS - QZSS - IRNSS` ?

> Beidou : [Clique ici](https://fr.wikipedia.org/wiki/Beidou)
>
> Galileo : [Clique ici](https://fr.wikipedia.org/wiki/Galileo_%28syst%C3%A8me_de_positionnement%29)
>
> GPS : [Clique ici](https://fr.wikipedia.org/wiki/Global_Positioning_System)
>
> Glonass : [Clique ici](https://fr.wikipedia.org/wiki/GLONASS)
>
> QZSS : [Clique ici](https://fr.wikipedia.org/wiki/Quasi-Zenith_Satellite_System)
>
> IRNSS : [Clique ici](https://fr.wikipedia.org/wiki/Indian_Regional_Navigation_Satellite_System)

![gpsLogo](images/gpslogo.png) **QUESTION 2** : Qu'est-ce que la norme `NMEA 0183` ?

> NMEA 0183 : [Clique ici](https://fr.wikipedia.org/wiki/NMEA_0183)

## Trame GGA

La trame GGA est très courante car elle fait partie de celles qui sont utilisées pour connaître la position courante (longitude, latitude et altitude) du récepteur GPS.

**TRAME GGA : $GPGGA,064036.289,4836.5375,N,00740.9373,E,1,04,3.2,200.2,M,,,,0000*0E**

+ $ : délimiteur de début de trame
+ GP : Id du “parleur” (GPS)
+ GGA : Type de trame (Global Positioning System Fixed Data)
+ **064036.289 : Trame envoyée à 06h40m36,289s (heure UTC : temps universel coordonné)**
+ **4836.5375,N : Latitude ddmm.mmmm -> 48,608958° Nord = 48°36’32.25" Nord** **(1)**
+ **00740.9373,E : Longitude dddmm.mmmm -> 7,682288° Est = 7°40’56.238" Est**
+ 1 : Type de positionnement (le 1 est un positionnement GPS)
+ 04 : Nombre de satellites utilisés pour calculer les coordonnées
+ 3.2 : Précision horizontale ou HDOP (Horizontal dilution of precision)
+ 200.2,M : Altitude 200,2, en mètres
+ ,,,,,0000 : D’autres informations peuvent être inscrites dans ces champs
+ *0E : Somme de contrôle de parité, un simple XOR sur les caractères précédents
+ <CR><LF> : délimiteur de fin de trame

> **(1)** :
> Généralement, on exprime les coordonnées géographiques dans le système sexagésimal, noté DMS pour degrés, minutes, secondes.
>
> Par exemple 49°30ʼ30ʼʼ pour 49 degrés, 30 minutes et 30 secondes.
>
> Une minute dʼangle vaut 1/60 degrés tandis quʼune seconde dʼangle vaut 1/3600 degrés.
>
> Il est également possible dʼutiliser les unités DM (Degré Minute) ou DD (Degré décimal) :
> + En DMS : 49°30ʼ30ʼʼ
> + En DM : 49°30,5ʼ
> + En DD : 49,5083° (généralement avec quatre décimales)


![gpsLogo](images/gpslogo.png) **QUESTION 3** : Dans la trame GGA quelle est l'unité utilisée pour les coordonnées géographiques.

![gpsLogo](images/gpslogo.png) **QUESTION 4** : Recherche sur le WEB, si il existe d'autre types de trame NMEA que GGA.

![gpsLogo](images/gpslogo.png) **QUESTION 5** : Décripter la trame suivante pour découvrir la tannière de DarkSATHI --> **$GPGGA,164833.47,5007.7309,N,00322.7028,E,1,09,3.2,200.2,M,,,,0000*0E**
**

> Il sera utile d'utiliser le site : [OpenStreetMap](https://www.openstreetmap.org) ou pour plus de précision [coordonnees-gps](https://www.coordonnees-gps.fr/)


# ![gpsTitre](images/gpstitre.png) Une simple photo pour te localiser !

![gpsLogo](images/gpslogo.png) **QUESTION 6** : Explique comment tu désactives la géolocalisation sur ton smartphone.

> [Android](https://www.androidpit.fr/comment-desactiver-localisation-applications-android)
>
> [Iphone](https://support.apple.com/fr-fr/HT207092)

![gpsLogo](images/gpslogo.png) **QUESTION 7** : Est-ce qu'il est possible de géolocaliser une photo que tu as prise depuis ton smartphone?
Si oui peux-tu expliquer comment on active ou désactive cette fonction?

![gpsLogo](images/gpslogo.png) **QUESTION 8** : Utilise le site https://www.dcode.fr/donnees-exif pour geolocaliser le lieu de prise de cette [photo](https://www.dropbox.com/s/m296hfqkeb84uxp/IMG_20191003_074251.jpg?dl=0).

> Dans la DropBox choisir en haut à droite le bouton télécharger.
>
> Si vous rencontrez des difficultés, vous pouvez lire [ici](EXIF.txt) les informations EXIF.

![gpsLogo](images/gpslogo.png) **QUESTION 9** : Recherche des informations sur la spécification ``EXIF`` de format de fichier pour les images utilisées par les appareils photographiques numériques.

--------------------------
## ![gpsTitre](images/gpstitre.png) Galileo en vidéo

![gpsLogo](images/gpslogo.png) **PROJET 2** : Visionne la vidéo ci-dessous et complète ton site sur le fonctionnement du GPS par la présentation du système Galileo.

[![image](images/galileo.png)](https://www.youtube.com/embed/e79tSIpLiDk)

## ![gpsTitre](images/gpstitre.png) À la marge ! (https://fr.wikipedia.org/wiki/D%C3%A9bris_spatial)

Évolution depuis le début de l'ère spatiale du nombre d'objets en orbite (> 10 cm en orbite basse et > 1 mètre sur les autres orbites) suivis par le réseau de surveillance américain USSTRATCOM. Ces 17 000 objets, pour lesquels on dispose des caractéristiques orbitales, ne représentent qu'une faible fraction des 500 000 objets d'une taille > 1 cm (Situation à la fin du premier du premier trimestre 2016).

![graphDebris](https://cdn.prod.website-files.com/6323b230ca04bcc5bf5aa7de/638a682d7e78fe1d9f880410_object%20mass.png)

## **Cartographie des principaux débris spatiaux en orbite terrestre basse.**

![graphDebris](https://cdn-s-www.bienpublic.com/images/AA032B3B-5096-4219-AFD7-F66F9DB9B2EA/MF_contenu/la-chute-de-debris-spatiaux-va-se-multiplier-selon-la-societe-astronomique-de-bourgogne-1572334558.jpg)

## **Cartographie vue d'au-delà de l'orbite géosynchrone.**

![graphDebris](https://img.20mn.fr/uBz9VVl9QK6q9c2SbQ-bWQ/1444x920_cartographie-vue-dela-orbite-geosynchrone)

