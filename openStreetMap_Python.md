# ![python_technologie](images/Python_technologie.png) La géolocalisation avec Python

Nous allons utiliser les cartes proposées par Open Street Map et le langage Python afin de générer des cartes personnalisées. Plus exactement, nous allons utiliser une bibliothèque Python nommée Folium. Une bibliothèque Python permet de rajouter des fonctionnalités au langage de base. Folium va donc nous permettre de créer nos propres cartes à partir des cartes proposées par Open Street Map.

![python_titre_bleu](images/Python_titre_bleu.png) **Créez un dossier et nommez-le OSM**

![python_titre_bleu](images/Python_titre_bleu.png) **Utilisez le logiciel thonny pour saisir le programme ci-dessous. Le fichier devra être enregistré dans le répertoire OSM (le fichier sera nommé watteau.py).**


```python
import folium
watteau = folium.Map(location = [50.358402, 3.531557], tiles = 'OpenStreetMap')
watteau.save('watteau.html')
```

![watteau1](images/watteauFolium_1.png)

Une fois le code ci-dessus exécuté, rendez-vous dans le répertoire OSM. Vous devriez trouver un fichier watteau.html. Double-cliquez sur ce fichier, cela devrait normalement ouvrir un navigateur web : la carte centrée sur le lycée Antoine Watteau est à votre disposition. Notez bien que nous avons une véritable carte et pas une simple image (il est possible de zoomer ou de se déplacer).


Le programme watteau.py est simple à comprendre :
+ La première ligne : import folium permet d'importer la bibliothèque folium afin de pouvoir l'utiliser
+ La deuxième ligne est le cœur de notre programme, nous définissons une variable ``watteau`` qui va contenir notre objet carte. ``folium.Map(location=[50.358402, 3.531557]``) génère cet objet carte, la carte sera centrée sur le point de latitude 50.358402 et de longitude 3.531557. Plus généralement nous avons ``folium.Map(location=[latitude, longitude])``. Il suffit donc de renseigner la bonne longitude et la bonne latitude pour que la carte soit centrée sur le point désiré.
+ La dernière ligne permet de générer la page HTML qui va permettre d'afficher la carte.
+ La carte est construite à partir de tuiles(``tiles``). Ce paramètre peut prendre les valeurs suivantes(entre autres) : "Cartodb Positron", "Cartodb dark_matter".


![python_titre_bleu](images/Python_titre_bleu.png) **Modifiez le programme watteau.py pour qu'il génère une carte centrée sur la ville de votre choix (la longitude et la latitude d'une ville sont facilement trouvables sur le web). On pourra renommer le fichier Python ainsi que le nom de la page HTML qui sera générée.**

---

### Il est possible d'obtenir un niveau de zoom différent en ajoutant un paramètre ``zoom_start``


![python_titre_bleu](images/Python_titre_bleu.png) **Saisissez et testez le programme ci-dessous (si vous désirez garder la carte précédente, n'oubliez pas de modifier la dernière ligne en changeant le nom du fichier HTML généré)**


```python
import folium
watteau = folium.Map(location = [50.358402, 3.531557], tiles = 'OpenStreetMap', zoom_start = 19)
watteau.save('watteau.html')
```

Plus la valeur de ``zoom_start`` sera grande et plus le zoom sera important.

![watteau2](images/watteauFolium_2.png)

---
### Afin de vraiment personnaliser la carte, il est possible d'ajouter des marqueurs sur la carte. 

### Un marqueur sera simplement défini par ses coordonnées (latitude et longitude).

![python_titre_bleu](images/Python_titre_bleu.png) **Saisissez et testez le programme ci-dessous**


```python
import folium
watteau = folium.Map(location = [50.358402, 3.531557], tiles = 'OpenStreetMap', zoom_start = 19)
folium.Marker([50.358402, 3.531557]).add_to(watteau)
watteau.save('watteau.html')
```

Nous avons uniquement ajouté la ligne ``folium.Marker``..., il faut juste renseigner les coordonnées souhaitées (ici 50.358402 pour la latitude et 3.531557 pour la longitude)

![watteau3](images/watteauFolium_3.png)

Il est possible d'ajouter plusieurs marqueurs sur une même carte, il suffira d'ajouter autant de ligne ``folium.Marker([latitude, longitude]).add_to(watteau)`` que de marqueurs désirés.

---
### Il est possible d'associer une information à un marqueur en ajoutant le paramètre popup.

![python_titre_bleu](images/Python_titre_bleu.png) **Saisissez et testez le programme ci-dessous**


```python
import folium
watteau = folium.Map(location = [50.358402, 3.531557], tiles = 'OpenStreetMap', zoom_start = 19)
folium.Marker([50.358402, 3.531557], popup = 'Lycée Antoine Watteau').add_to(watteau)
watteau.save('watteau.html')
```

Il suffit de cliquer sur le marqueur pour que l'information définie par le paramètre popup apparaisse à l'écran (ici en cliquant sur le marqueur nous verrons donc apparaitre : Lycée Antoine Watteau).

![watteau4](images/watteauFolium_4.png)

### Création d'une zone en forme de cercle : Pouvez-vous utiliser ce marker pour faire l'activité un trèsor à Valenciennes?

```python
import folium
watteau = folium.Map(location = [50.358402, 3.531557], tiles = "OpenStreetMap", zoom_start = 19)
folium.Marker([50.358402, 3.531557], popup = 'Lycée Antoine Watteau').add_to(watteau)
folium.CircleMarker(location=(50.358402, 3.531557),radius=100, fill_color='blue').add_to(watteau)
watteau.save('watteau.html')
```

![watteau5](images/watteauFolium_5.png)
-----------------------
##  ![projet](images/projet.png) Réaliser une carte détaillée du Lycée Antoine Watteau.

### Il est possible de personnaliser les popup.

```python
icon = folium.features.CustomIcon('monImage.png', icon_size=(tailleX,tailleY))
folium.Marker([50.358402, 3.531557],
              popup='icon',
              icon=icon
              ).add_to(watteau)
```
