# ![gpsTitre](images/gpstitre.png) **Un trésor à Valenciennes**

Un trésor a été caché quelque-part à Valenciennes. Vous êtes par **équipe de trois** et devez le retrouver le plus
rapidement possible. Le trésor émet un signal se propageant à la vitesse du son (340 m/s) et détectable à plus de 1km.

Dans tous les calculs, on arrondira les distances à 0,01 km et les temps à 0,001 seconde.

> **Pour cette activité il faut une calculatrice : [Numworks en ligne](https://www.numworks.com/fr/simulateur/)**

![Watteau](images/GPS_carte_Watteau.png)

![gpsLogo](images/gpslogo.png) Pour avoir un ordre d’idée des distances, on suppose que le trésor est situé au Carré des Art.

Combien de temps mettrait le signal pour parvenir au lycée Antoine Watteau ?

![gpsLogo](images/gpslogo.png) Le trésor est désormais caché dans Valenciennes. Le signal met 0,302s pour parvenir au musée des Beaux-Arts de Valenciennes. Calculer la distance qui sépare le trésor du musée.

![gpsLogo](images/gpslogo.png) Sachant que le trésor commencera à émettre aujourd'hui à 8h (un signal toutes les 30 mn) et que vous êtes
3 dans l’équipe, quelle stratégie allez-vous adopter pour localiser le trésor?

![gpsLogo](images/gpslogo.png) Compléter le tableau des distances au trésor en mètre et des distance sur la carte en fonction de l'échelle: 

|Lieu| Temps en secondes| Distances au trésor(réelle en m)|Distances au trésor sur la carte|
|:--:|:--:|:--:|:--:|
Lycée Notre Dame|0.575| ||
Salle du Hainaut|0.903| ||
Architecte Sakariba|0.528| ||

![gpsLogo](images/gpslogo.png) Localiser le trésor en vous répartissant le travail : **Utiliser [GEOGEBRA](https://www.geogebra.org/calculator) et ouvrir le fichier [tresor.ggb](src/tresor.ggb) pour localiser le trésor.**

> Nos appareils GPS (smartphones, véhicules …) utilisent également le
> principe de calcul d’une distance à partir du temps de propagation d’une onde. 
> Ils calculent le temps que met un signal pour faire un aller depuis un satellite.
> Quatre satellites visibles suffisent pour obtenir la position et l’altitude du récepteur GPS.
> Les algorithmes implantés dans les appareils GPS calculent les informations numériques : altitude, coordonnées en latitude et longitude … et les stockent dans la mémoire de l’appareil (votre localisation !).
> Si vous autorisez leur utilisation (data), ces informations numériques sont envoyées aux applications qui le souhaitent sous la forme d’une trame (suite de bits représentants des informations codées en binaire, souvent des caractères).
> La norme NMEA (National Marine Electronics Association) définit une trentaine de trames GPS différentes.
> La trame au format GGA est aujourd’hui une des plus utilisée.


**À SUIVRE : Le GPS comment ça marche !**
